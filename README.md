# Adversarial-Geometry-Aware-Human-Motion-Prediction
This is a non-official implementation of the paper 
Gui _et al_., ["Adversarial Geometry-Aware Human Motion Prediction"](http://openaccess.thecvf.com/content_ECCV_2018/papers/Liangyan_Gui_Adversarial_Geometry-Aware_Human_ECCV_2018_paper.pdf). In ECCV 2018

## Dependencies

`numpy`: For matrix operation

`pytorch 1.1.0` : main framework for implementing the neural network, make sure `GPU` verision is installed.

`matplotlib 3.1.0`: visulization for human motion skeleton

`imageio`: saving motion sequence in GIF annimation

## How the Code Work and Visualization
run `python3 main.py`

Without any modification of the main.py, the code is in **"debug"** mode. By debug mode, the model is both training and validating in training data, which cause **overfitting**. This help me confirm that the model I implement is correct in the sense it can memorize the data.

Once train for a 100 epoch, one should see the visulization of the human skeleton and a saved GIF annimatation (/images/motion.gif) like the follwoing:
![Human Motion Visualization](./motion-adv.gif)

The first skeleton color saturated RGB is the observed sequence with the duration of 4 seconds. After that, two skeletons are drawn in overlayed, the 1-second prediction of the furture motion(lighter RGB skeleton) and the ground truth (black skeleton)

## Model Implementation

* `Predictor`
a [Seq2Seq](src/model/Model.py) model based on [On human motion prediction using recurrent neural networks](https://arxiv.org/pdf/1705.02445.pdf)
* `Fidelity Discriminator`
a [recurrent neural network]((src/model/Model.py)) model with sigmoid output, it is named as Discrimator in .py
* `Continuity Discriminator`
Same as Fidelity discriminator.
## Note
- connect to uni-bonn
- ```ssh wwang@login.iai.uni-bonn.de```
- connect to workstation
- ```ssh weiwang@cvg10``` 
- In Martinetz Code [31], feed 2 seconds of motion to the encoder, and predict either 1 second (for long-term experiments) or 400 milliseconds (for short-term prediction)

## Code Reference
* [On human motion prediction using recurrent neural networks](https://github.com/enriccorona/human-motion-prediction-pytorch)
