import numpy as np
import os

from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
import matplotlib.pyplot as plt
import imageio

def drawSequence(tgtSequence, gtSequence = None, srcSequence = None, isSaveGIF = False, fileName = ""):
  nSrcFrames = 0 if srcSequence is None else srcSequence.shape[0]
  nTgtFrames = tgtSequence.shape[0]
  totalFrames = nSrcFrames + nTgtFrames
  if gtSequence is not None:
    isParallel = True
    tgtSequence = np.stack((tgtSequence, gtSequence),axis=2)
  else:
    tgtSequence = np.expand_dims(tgtSequence, axis=2)
    isParallel = False
  
  # === Plot and animate ===
  fig = plt.figure()
  ax = plt.gca(projection='3d')
  player = MotionSequencePlayer(ax)
  gif = []
  for frame in range(0,totalFrames):
    if frame < nSrcFrames:
      player(srcSequence[frame,:],lcolor= "#0000ff", rcolor="#ff0000")
    else:
      if isParallel:
        if nSrcFrames != 0:
          player(tgtSequence[frame - nSrcFrames,:,:],isParallel = isParallel)
        else:
          player(tgtSequence[frame,:,:],isParallel = isParallel)
      else:
        if nSrcFrames != 0:
          player(tgtSequence[frame - nSrcFrames,:],isParallel = isParallel)
        else:
          player(tgtSequence[frame,:],isParallel = isParallel)

    plt.show(block=False)
    fig.canvas.draw()
    if isSaveGIF:
        image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
        image  = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        gif.append(image)
    plt.pause(0.001)
  plt.close(fig)
  if isSaveGIF:
    # imageio.mimsave('./images/motion.gif',gif, fps= 25)
    imageio.mimsave("./images/{0}.gif".format(fileName),gif, fps= 25)
    
class MotionSequencePlayer:
  def __init__(self, ax, lcolor="#3498db", rcolor="#e74c3c"):
    """
    Create a 3d pose visualizer that can be updated with new poses. 
    """
    # Start and endpoints of our representation
    self.I   = np.array([1,2,3,1,7,8,1, 13,14,15,14,18,19,14,26,27])-1
    self.J   = np.array([2,3,4,7,8,9,13,14,15,16,18,19,20,26,27,28])-1
    # Left / right indicator
    self.LR  = np.array([1,1,1,0,0,0,0, 0, 0, 0, 0, 0, 0, 1, 1, 1], dtype=bool)
    self.ax = ax
     # Make connection matrix
    self.plots = []
    vals = np.zeros((32, 3)) # tero motion, all joints located at origin
    for i in np.arange( len(self.I) ):
      x = np.array( [vals[self.I[i], 0], vals[self.J[i], 0]] )
      y = np.array( [vals[self.I[i], 1], vals[self.J[i], 1]] )
      z = np.array( [vals[self.I[i], 2], vals[self.J[i], 2]] )

      self.plots.append(self.ax.plot(x, y, z, lw=2, c=lcolor if self.LR[i] else rcolor))
      self.plots.append(self.ax.plot(x, y, z, lw=2, c=lcolor if self.LR[i] else rcolor))

    self.ax.set_xlabel("x")
    self.ax.set_ylabel("y")
    self.ax.set_zlabel("z")

  def __call__(self, channels, lcolor = "#3498db", rcolor = "#e74c3c", isParallel = False):
    nSkeleton = 1 if not isParallel else 2
    if not isParallel:
      vals = np.reshape(channels, (32, -1))
    else: 
      vals = np.concatenate((np.reshape(channels[:,1], (32, -1)),
                             np.reshape(channels[:,0], (32, -1))), axis=1)
    # channel2 = np.reshape(channel2, (32, -1))
    for j in reversed(range(0, nSkeleton)):
      if isParallel and j == nSkeleton-1:
        lcolor = "#3498db"
        rcolor = "#e74c3c"
      if isParallel and j == 0:
        lcolor = "000000"
        rcolor = "000000"
      for i in np.arange( len(self.I) ):
        x = np.array( [vals[self.I[i], 0 + 3*j], vals[self.J[i], 0 + 3*j]] )
        y = np.array( [vals[self.I[i], 1 + 3*j], vals[self.J[i], 1 + 3*j]] )
        z = np.array( [vals[self.I[i], 2 + 3*j], vals[self.J[i], 2 +3*j]] )
        self.plots[j*len(self.I) + i][0].set_xdata(x)
        self.plots[j*len(self.I) + i][0].set_ydata(y)
        self.plots[j*len(self.I) + i][0].set_3d_properties(z)
        self.plots[j*len(self.I) + i][0].set_color(lcolor if self.LR[i] else rcolor)
      
    self.ax.set_xlabel("x")
    self.ax.set_ylabel("y")
    self.ax.set_zlabel("z")
    
    r = .8
    xroot, yroot, zroot = vals[0,0], vals[0,1], vals[0,2]
    self.ax.set_xlim3d([-r+xroot, r+xroot])
    self.ax.set_zlim3d([-r+zroot, r+zroot])
    self.ax.set_ylim3d([-r+yroot, r+yroot])


    