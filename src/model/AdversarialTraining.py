import torch

def trainDiscriminators(fidD, conD, predictor, fidOptimiser, conOptimiser, srcSeq, tgtSeq, factor):
    predictor.train()
    predictor.decoder.drop.eval()
    N = srcSeq.size(1)
    device = srcSeq.device
    realLabel = 1
    fakeLabel = 0
    advLoss = torch.nn.BCELoss() 
    fidOptimiser.zero_grad()
    conOptimiser.zero_grad()

    ## Train with all-real batch 
    # Compose a real batch
    fidRealBatch = tgtSeq
    conRealBatch = torch.cat((srcSeq,tgtSeq),0)
    label = applySoftLabel(realLabel, N, device)
    # Forward pass real batch through D
    fidOutput = fidD(fidRealBatch).view(-1)
    conOutput = conD(conRealBatch).view(-1)
    fidLossReal = advLoss(fidOutput, label) * factor
    conLossReal = advLoss(conOutput, label) * factor
    fidLossReal.backward()
    conLossReal.backward()
    
    ## Train with all-fake batch
    # Compose a fake batch
    fidFakeBatch = predictor(srcSeq).detach()
    conFakeBatch = torch.cat((srcSeq, predictor(srcSeq).detach()),0)
    label = applySoftLabel(fakeLabel, N, device)
    # Foward pass fake batch through D
    fidOutput = fidD(fidFakeBatch).view(-1)
    conOutput = conD(conFakeBatch).view(-1)
    fidLossFake = advLoss(fidOutput, label) * factor
    conLossFake = advLoss(conOutput, label) * factor
    fidLossFake.backward()
    conLossFake.backward()

    fidOptimiser.step()
    conOptimiser.step()
    return {"fidLossReal": fidLossReal.item(), "conLossReal": conLossReal.item(), "fidLossFake": fidLossFake.item(), "conLossFake":conLossFake.item()}

def applySoftLabel(label, n, device):
    if label == 1:
        return 0.1 * torch.rand(n).to(device) + 0.9
    elif label == 0:
        return 0.1 * torch.rand(n).to(device)
    
def trainPredictor(fidD, conD, predictor, optimiser, srcSeq, tgtSeq, advFactor):
    N = srcSeq.size(1)
    device = srcSeq.device
    realLabel = 1
    advLoss = torch.nn.BCELoss() 
    label = torch.full((N,), realLabel, device=device)  # fake labels are real for generator cost
    errZ = 0.0
    errP = 0.0
    
    # Compose a fake batch
    fidFakeBatch = predictor(srcSeq)
    conFakeBatch = torch.cat((srcSeq, predictor(srcSeq)),0)
    # Foward pass fake batch through D
    fidOutput = fidD(fidFakeBatch).view(-1)
    conOutput = conD(conFakeBatch).view(-1)

    errZ =  advLoss(fidOutput, label)
    errZ += advLoss(conOutput, label)
    errP = torch.mean((fidFakeBatch-tgtSeq)**2) + errZ * advFactor

    errP.backward()
    optimiser.step()
    
    return errP.item(), errZ.item()



    