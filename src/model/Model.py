from __future__ import absolute_import, division, print_function
import random

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim

class Encoder(nn.Module):
    def __init__(self, inputSize, hiddenSize):
        super(Encoder, self).__init__()
        self.inputSize = inputSize
        self.hiddenSize = hiddenSize
        self.gru = nn.GRU(self.inputSize, self.hiddenSize) # kargs = (input size of x, hidden size, num layer = 1, bias = true, dropout = 0, bidirectional = true)

    def forward(self, x, prevHiddenState):
        output, hiddenState = self.gru(x, prevHiddenState)
        return output, hiddenState


class Decoder(nn.Module):
    def __init__(self, inputSize, hiddenSize, outputSize):
        super(Decoder, self).__init__()
        self.hiddenSize = hiddenSize
        self.gru = nn.GRU(inputSize, hiddenSize)
        self.fc = nn.Linear(hiddenSize, outputSize)
        self.drop = nn.Dropout(p=0.3)

    def forward(self, x, prevHiddenState):
        output, hiddenState = self.gru(x, prevHiddenState)
        output = self.fc(self.drop(output)) + x # adding x to the output implies residual connection
        return output, hiddenState

class Predictor(nn.Module):
    def __init__(self, inputSize, enHiddenSize = 1024, deHiddenSize = 1024, numTgtFrames = 25, device = "cpu"):
        super(Predictor, self).__init__()
        self.device = device
        self.numTgtFrames = numTgtFrames
        self.inputSize = inputSize
        self.enHiddenSize = enHiddenSize
        self.encoder = Encoder(self.inputSize, enHiddenSize)
        self.decoder = Decoder(self.inputSize, deHiddenSize, self.inputSize)

    def forward(self, x):
        _, encoderState = self.encoder(x, initHidden(x.size(1), self.enHiddenSize, self.device))
        decoderOutput = x[-1,:,:].view(1 ,x.size(1), self.inputSize)
        decoderState = encoderState
        decoderOutputs = []
        for j in range(0, self.numTgtFrames):
            decoderOutput, decoderState = self.decoder(decoderOutput, decoderState)
            decoderOutputs.append(decoderOutput)
        decoderOutputs = torch.cat(decoderOutputs, 0)
        return decoderOutputs

class Discriminator(nn.Module):
    def __init__(self, inputSize, hiddenSize, device):
       super(Discriminator, self).__init__()
       self.device = device
       self.hiddenSize = hiddenSize
       self.gru = nn.GRU(inputSize, hiddenSize)
       self.linear = nn.Linear(hiddenSize, 1)
       self.sigmoid = nn.Sigmoid()
    
    def forward(self, x):
        _, encoderState = self.gru(x, initHidden(x.size(1), self.hiddenSize, device=self.device))
        output = self.linear(encoderState)
        output = self.sigmoid(output)
        return output

def initHidden(batchNumber, hiddenSize, device="cpu"):
        return torch.zeros(1, batchNumber, hiddenSize).to(device)