from __future__ import print_function, division
import re
import os
import torch
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
# from torch.nn.utils.rnn import pad_sequence, pack_padded_sequence

class Human36mXYZ(Dataset):
    """
    A Eulidean (xyz) representation of Human3.6m dataset converted from the exp. map.
    Note that all sequences have frame rate of 50Hz, but the output of the dataloader downsample the sequence to 25Hz  
    """

    def __init__(self, rootDir, subjects, actions, numSrcFrame, numTgtFrame, debug):
        """
        Args:
            rootDir: string, root directory contains different sequences of motion from different subjects, e.g. rootDir/S1_walking_1.txt
            isSampleSubseq: bool, resample the sequence to augment the number of sequence in the dataset
            numSrcFrame: int, number of source frame being used in training.
            numTrgFrame: int, number of target frame being used in validating prediction. 
        """
        self.rootDir = rootDir
        self.dim = 96 # 32 (joints) * 3 (xyz channel) = 96
        self.numSrcFrame = numSrcFrame
        self.numTgtFrame = numTgtFrame
        self.motionIDs = []
        self.sequences = {}
        self.subsequentList = {}
        self.datasetSize = 0
        

        for fileName in os.listdir(rootDir):
            result = re.search( r'(\d*)_(.*)_(\d*)', fileName, re.I) # the fileName has the structure of S{subjectNumber}_{action}_{subactionNumber}
            subject = int(result.group(1))
            action = result.group(2)
            subaction = int(result.group(3))

            if action in actions and subject in subjects:
                print("Initialize pre-compute starting frame (subject: {0}, action: {1}, subaction: {2} )".format(subject, action, subaction))
                self.motionIDs.append((subject, action, subaction)) # a dictionary that record the pre-compute starting frame position of subsequence in a sequence
                self.sequences[(subject, action, subaction)] = np.loadtxt(
                    os.path.join(self.rootDir, "S{0}_{1}_{2}.txt".format(subject,action,subaction)))
                if debug:
                    self.subsequentList[(subject, action, subaction)] = \
                    np.arange(200, 200 + 24 * 2, 2,dtype=int)
                    self.datasetSize += self.subsequentList[(subject, action, subaction)].shape[0]
                    break 
                else:     
                    self.subsequentList[(subject, action, subaction)] = \
                        np.arange(0, self.sequences[(subject, action, subaction)].shape[0] - (self.numSrcFrame + self.numTgtFrame) * 2, 2,dtype=int)
                    self.datasetSize += self.subsequentList[(subject, action, subaction)].shape[0] 
            else:
                continue

        # self.motionIDs.sort(key = lambda x:(x[0], x[1]))s
    def __len__(self):
        return self.datasetSize
    def __getitem__(self,idx):
        n = 0
        normalizeIdx = idx
        for motionID in self.motionIDs:
            normalizeIdx = idx - n
            n += self.subsequentList[motionID].shape[0]
            if idx-n < 0:
                break
        startingFrames = self.subsequentList[motionID] 
        
        start = startingFrames[normalizeIdx] 
        subsequence = torch.from_numpy(
            self.sequences[motionID][start:start + (self.numSrcFrame + self.numTgtFrame)*2: 2, :])
        return motionID, subsequence

class Collater:
    """
    Compose a batch of compose by subsequences
    """
    def __init__(self, numSrcFrame, numTgtFrame, device, inputSize = 96):
        self.numSrcFrame = numSrcFrame
        self.numTgtFrame = numTgtFrame
        self.inputSize = inputSize
        self.device = device
    def __call__(self,batch):
        """
        Args:
            batch: list, where each element is a tuple of (motionsID, subsequences)
        Outputs:
            srcFrames: a batch contains 50-frames subsequences
            tgtFrames: a batch conatnis 25-frames subsequences after the source frames 
        """  
        batchSize = len(batch)
        batchStats = {} # key: motion, val: number of example
        srcFrames = torch.zeros(self.numSrcFrame, batchSize, self.inputSize)
        tgtFrames = torch.zeros(self.numTgtFrame, batchSize, self.inputSize)
        

        for i, item in enumerate(batch):
                if item[0] not in batchStats:
                    batchStats[item[0]] = 1
                else:
                    batchStats[item[0]] += 1
                srcFrames[:,i,:] = item[1][0:self.numSrcFrame,:]
                tgtFrames[:,i,:] = item[1][self.numSrcFrame: self.numSrcFrame + self.numTgtFrame,:]

        return srcFrames.to(self.device), tgtFrames.to(self.device), batchStats

def getLoader(rootDir, subjects, actions, numSrcFrame = 50, numTgtFrame = 25, batchSize = 8, shuffle = False, device = "cpu", debug = False):
    # def collate_fn(batch):
    #     motionIDs = []
    #     sequences = []
    #     lengths = []
    #     batch.sort(key = lambda x:x[1].shape[0], reverse=True)
    #     for item in batch:
    #         motionIDs.append(item[0])
    #         sequences.append(torch.from_numpy(item[1]))
    #         lengths.append(item[1].shape[0])
        
    #     if (len(batch[0][1].shape) == 2):
    #         padded_sequences = pad_sequence(sequences)
    #         return pack_padded_sequence(padded_sequences,lengths)
        # elif(len(batch[0][1].shape) == 3):
        #     # is a 3 dimension np array with dimension (i,j,k), where
        #     # i : represent the index of a subsequence
        #     # j : represent the index of a frame
        #     # k : represent the the joint
        #     pad_sequence
    dataset = Human36mXYZ(rootDir, subjects, actions, numSrcFrame, numTgtFrame, debug)
    data_loader = torch.utils.data.DataLoader(dataset=dataset,
                                              batch_size=batchSize,
                                              collate_fn=Collater(numSrcFrame,numTgtFrame, device = device),
                                              shuffle=shuffle,
                                              num_workers=0)
    return data_loader
    
