from dataset.Human36XYZ import getLoader
from model.Model import Predictor, Discriminator
from model.AdversarialTraining import trainPredictor, trainDiscriminators
from utils import Visualization as vis
import torch.optim as optim
import torch
import matplotlib

def updateNet(net, optimiser, X, Y):
    optimiser.zero_grad()
    Yhat = net(X)
    error = torch.mean((Yhat - Y)**2)
    error.backward()
    optimiser.step()

if __name__ == "__main__":
    rootDir = "./data"
    actions = ["walking"]
    actions = ["directions", "discussion", "eating", "greeting", "phoning",
              "posing", "purchases", "sitting", "sittingdown", "smoking",
              "takingphoto", "waiting", "walking", "walkingdog", "walkingtogether"]
    # subjects = [1]
    subjects = [1, 6, 7, 8, 9, 11]
    batchSize = 16
    humanSize = 96
    hiddenSize = 1024
    numSrcFrames = 50
    numTgtFrames = 25
    
    # CUDA
    isGPUavailable = torch.cuda.is_available()
    device = torch.device("cuda" if isGPUavailable else "cpu")
    # Data
    debug = False
    isSaveGIF = True
    dataloader = getLoader(rootDir, subjects , actions, shuffle=True, batchSize=batchSize, device = device, debug= debug)

    # Model
    predictor = Predictor(inputSize = humanSize, 
                          enHiddenSize = 1024,
                          deHiddenSize = 1024, 
                          numTgtFrames=25,
                          device = device).to(device)
    
    fidDiscriminator = Discriminator(inputSize = humanSize, 
                                     hiddenSize = 1024,
                                     device = device).to(device)
    conDiscriminator = Discriminator(inputSize = humanSize, 
                                     hiddenSize = 1024,
                                     device = device).to(device)
    # Optimser paramter
    pLearningRate = 0.0001
    conLearningRate = 0.0001
    fidLearningRate = 0.0001
    adverFactor = 0.6
    predictorOptimiser = optim.Adam(predictor.parameters(), lr=pLearningRate)
    
    fidelityOptimiser = optim.Adam(fidDiscriminator.parameters(), lr=fidLearningRate)
    continuityOptimiser = optim.Adam(conDiscriminator.parameters(), lr=conLearningRate)
    
    # Training
    numEpoch = 1000000
    maxDisIters = 1
    # TODO: Load the testing motion here, since we are going to test the model on it for every iteration
    for epoch in range(0, numEpoch):
        for iteration, batch in enumerate(dataloader):
            
            for k in range(0, maxDisIters):
                srcFrames, tgtFrames, _ = batch
                predictorOptimiser.zero_grad()
                # TODO: Implement gradient clipping
                errorD = trainDiscriminators(fidDiscriminator, conDiscriminator, predictor, 
                                            fidelityOptimiser, continuityOptimiser, srcFrames, tgtFrames, adverFactor)
            predictor.train()
            srcFrames, tgtFrames, _ = batch
            predError, _= trainPredictor(fidDiscriminator, conDiscriminator, predictor, predictorOptimiser, srcFrames, tgtFrames, adverFactor)

             # Output training stats
            print('[%d/%d][%d/%d]\tLoss_G:%.4f\tfidD(x): %.4f\tconD(x): %.4f\tfidD(G(z)): %.4f\tconD(G(z)): %.4f'
                % (epoch, numEpoch, iteration, len(dataloader),
                    predError, errorD["fidLossReal"], errorD["conLossReal"], errorD["fidLossFake"], errorD["conLossFake"]))

            # TODO: Checkpoint: save model, log training error and testing error, and save the qualititative result here
            if iteration % 200 == 1:
                predictor.eval()
                vis.drawSequence(
                    predictor(srcFrames)[:,0,:].clone().cpu().detach().numpy(),
                    gtSequence = tgtFrames[:,0,:].clone().cpu().detach().numpy(),
                    srcSequence = srcFrames[:,0,:].clone().cpu().detach().numpy(),
                    isSaveGIF=isSaveGIF,
                    fileName="disIter_{2}_epoch_{0}_iter_{1}".format(epoch, iteration, maxDisIters))
    


